#!/bin/bash

inotifywait -m /srv/processing -e create -e moved_to |
    while read path action file; do
        if [[ "$file" =~ [Ff]amily.[Gg]uy ]]; then
            ffmpeg -i "/srv/processing/$file" -filter:v 'scale=1280x720,setdar=16:9,setsar=1:1,fps=25' -c:a aac -b:a 191k "/tmp/$file.mp4" < /dev/null && mv -vf "/tmp/$file.mp4" "/srv/rename/fg/$file.mp4" && rm -f "/srv/processing/$file"
        elif [[ "$file" =~ [Ss]impsons ]]; then
            ffmpeg -i "/srv/processing/$file" -filter:v 'scale=1280x720,setdar=16:9,setsar=1:1,fps=25' -c:a aac -b:a 191k "/tmp/$file.mp4" < /dev/null && mv -vf "/tmp/$file.mp4" "/srv/rename/simp/$file.mp4" && rm -f "/srv/processing/$file"
        elif [[ "$file" =~ [Tt]railer.[Pp]ark.[Bb]oys ]]; then
            ffmpeg -i "/srv/processing/$file" -filter:v 'scale=1280x720,setdar=16:9,setsar=1:1,fps=25' -c:a aac -b:a 191k "/tmp/$file.mp4" < /dev/null && mv -vf "/tmp/$file.mp4" "/srv/rename/tpb/$file.mp4" && rm -f "/srv/processing/$file"
        elif [[ "$file" =~ [Ss]outh.[Pp]ark ]]; then
            ffmpeg -i "/srv/processing/$file" -filter:v 'scale=1280x720,setdar=16:9,setsar=1:1,fps=25' -c:a aac -b:a 191k "/tmp/$file.mp4" < /dev/null && mv -vf "/tmp/$file.mp4" "/srv/rename/sp/$file.mp4" && rm -f "/srv/processing/$file"
        elif [[ "$file" =~ [Rr]ick.[Aa]nd.[Mm]orty ]]; then
            ffmpeg -i "/srv/processing/$file" -filter:v 'scale=1280x720,setdar=16:9,setsar=1:1,fps=25' -c:a aac -b:a 191k "/tmp/$file.mp4" < /dev/null && mv -vf "/tmp/$file.mp4" "/srv/rename/rm/$file.mp4" && rm -f "/srv/processing/$file"
        fi
    done
