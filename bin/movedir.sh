#!/bin/bash

inotifywait -m /srv/rename/simp /srv/rename/fg /srv/rename/tpb /srv/rename/sp /srv/rename/rm -e create -e moved_to |
    while read -r path action file; do
        if [[ "$file" =~ [Ff]amily.[Gg]uy ]]; then
            for i in /srv/rename/fg/*; do mv "$i" "$(echo "$i" | tr -d "'")"; done ; mv -f /srv/rename/fg/"$file" /srv/output/familyguy/
        elif [[ "$file" =~ [Ss]impsons ]]; then
            for i in /srv/rename/simp/*; do mv "$i" "$(echo "$i" | tr -d "'")"; done ; mv -f /srv/rename/simp/"$file" /srv/output/simpsons/
        elif [[ "$file" =~ [Tt]railer.[Pp]ark.[Bb]oys ]]; then
            for i in /srv/rename/tpb/*; do mv "$i" "$(echo "$i" | tr -d "'")"; done ; mv -f /srv/rename/tpb/"$file" /srv/output/trailerparkboys/
        elif [[ "$file" =~ [Ss]outh.[Pp]ark ]]; then
            for i in /srv/rename/sp/*; do mv "$i" "$(echo "$i" | tr -d "'")"; done ; mv -f /srv/rename/sp/"$file" /srv/output/southpark/
        elif [[ "$file" =~ [Rr]ick.[Aa]nd.[Mm]orty ]]; then
            for i in /srv/rename/rm/*; do mv "$i" "$(echo "$i" | tr -d "'")"; done ; mv -f /srv/rename/rm/"$file" /srv/output/rickandmorty/
        fi
    done
